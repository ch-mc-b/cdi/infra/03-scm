## Übungen: Source Code Management (Versionsverwaltung)

Eine [Versionsverwaltung](https://de.wikipedia.org/wiki/Versionsverwaltung) ist ein System, das zur Erfassung von Änderungen an Dokumenten oder Dateien verwendet wird. Alle Versionen werden in einem Archiv mit Zeitstempel und Benutzerkennung gesichert und können später wiederhergestellt werden. Versionsverwaltungssysteme werden typischerweise in der Softwareentwicklung eingesetzt, um Quelltexte zu verwalten. Versionsverwaltung kommt auch bei Büroanwendungen oder Content-Management-Systemen zum Einsatz.


### Hauptaufgaben

* Protokollierungen der Änderungen: Es kann jederzeit nachvollzogen werden, wer wann was geändert hat.
* Wiederherstellung von alten Ständen einzelner Dateien: Somit können versehentliche Änderungen jederzeit wieder rückgängig gemacht werden.
* Archivierung der einzelnen Stände eines Projektes: Dadurch ist es jederzeit möglich, auf alle Versionen zuzugreifen.
* Koordinierung des gemeinsamen Zugriffs von mehreren Entwicklern auf die Dateien.
* Gleichzeitige Entwicklung mehrerer Entwicklungszweige (engl. Branches) eines Projektes.

### Übungen

Die Übungen finden in der [Git/Bash](https://git-scm.com/downloads) statt. 

Vor den Übungen muss Mail und Username gesetzt werden:

    git config --global user.name "<username>"
    git config --global user.email "<your_email>"

## Übung(en): Git Grundlagen

Clont das Repository

    git clone https://gitlab.com/ch-mc-b/cdi/infra/03-scm.git
    
Wechselt in das Projektverzeichnis und editiert ein paar der Dateien.
    
    cd 03-scm
    
    # Editieren oder neu Anlegen von Dateien
    notepad README.md
    touch test.txt
    
Anschliessend können wir uns die Änderungen (status oder diff) anschauen, die geänderten Dateien zum Commit (add -A) vormerken und diese dann Committen.

    git status
    git diff
    git add -A
    git commit -m "Meine Aenderungen"
    
Zum Schluss können wir die Änderungen anschauen

    git log

### Links

* [Git Buch](https://git-scm.com/book/de/v2)


